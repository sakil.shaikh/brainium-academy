jQuery(document).ready(function($){

  // mouse move banner image move
    // var movementStrength = 25;
    // var height = movementStrength / $(window).height();
    // var width = movementStrength / $(window).width();
    // $(".main-img").mousemove(function(e){
    //           var pageX = e.pageX - ($(window).width() / 2);
    //           var pageY = e.pageY - ($(window).height() / 2);
    //           var newvalueX = width * pageX * -1 - 25;
    //           var newvalueY = height * pageY * -1 - 50;
    //           $('.main-img').css("transform", "translate(" + newvalueX + "%, " + newvalueY + "%)");

    // });


  $('ul.tabs li').on('click', function(){
    // get the data attribute
    var tab_id = $(this).attr('data-tab');
    // remove the default classes
    $('ul.tabs li').removeClass('current');
    $('.tab-content').removeClass('current');
    // add new classes on mouse click
    $(this).addClass('current');
    $('#'+tab_id).addClass('current');
  });


  // testimonial slider
  $('.testimonial-slider').owlCarousel({
      loop: true,
      responsiveClass: true,
      dots: false, 
      nav: true,         
      margin: 0,
      autoplayTimeout: 4000,
      smartSpeed: 1000,
      center: true,
      navText: ['←', '→'],
      responsive: {
          0: {
              items: 1,
          },
          600: {
              items: 2
          },
          1200: {     
              items: 3
          }
      }
  });

});